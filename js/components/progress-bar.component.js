angular.module('app')
.component('progressBar', {
    bindings: {
        value: '<'
    },
    template: `
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="{{$ctrl.value}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$ctrl.value + '%'}}">
                <span ng-show="$ctrl.value > 0">{{$ctrl.value + '%'}}</span>
            </div>
        </div>
    `,
    controller: function() {}
});