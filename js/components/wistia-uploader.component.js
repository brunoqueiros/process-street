angular.module('app')
.component('wistiaUploader', {
    bindings: {
        onUpload: '<'
    },
    template: `
        <form id="fileupload" action="" method="POST" enctype="multipart/form-data">
            <input type="file" name="file">
            <hr />
            <progress-bar value="$ctrl.progressPercentual"></progress-bar>
        </form>
    `,
    controller: function($scope) {
        this.progressPercentual = 0;

        const api_password = '0b07d20ecd516e56139ed8be14f522698aab909477d3ccac23f9303708382909';
        const url = 'https://upload.wistia.com';

        $('#fileupload').fileupload({
            url: url,
            type: 'POST',
            formData: {
                'api_password': api_password
            },
            done: (e, data) => {
                this.onUpload(data.result.hashed_id);
            },
            fail: function() {
                console.log('something wrong happened :(');
            },
            progressall: (e, data) => {
                this.progressPercentual = parseInt(data.loaded / data.total * 100, 10);
                $scope.$apply();
            }
        });
    }
});