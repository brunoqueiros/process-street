angular.module('app')

.component('wistiaPlayer', {
    bindings: {
        id: '<'
    },
    template: `
        <div
            ng-show="$ctrl.id"
            class="wistia_embed wistia_async_{{$ctrl.id}}"
            videoFoam="true"
            playerColor="ff0000"
            style="height:360px;"></div>
    `,
    controller: function() {}
});