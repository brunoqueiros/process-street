angular.module('app')
.component('app', {
    template: `
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <wistia-uploader on-upload="$ctrl.updateHashedId"></wistia-uploader>
                <hr />
                <wistia-player ng-show="$ctrl.hashedID !== ''" id="$ctrl.hashedID"></wistia-player>
            </div>
        </div>
    `,
    controller: function($scope) {
        this.hashedID = '';

        this.updateHashedId = (hashedID) => {
            this.hashedID = hashedID;
            $scope.$apply();
        };
    }
});